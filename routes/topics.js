var express = require('express');
var router = express.Router();

var authentication = require('../utils/authentication');
var Models = require('../models');

// Huom! Kaikki polut alkavat polulla /topics

// GET /topics
router.get('/', function(req, res, next) {
    // Hae kaikki aihealueet tässä (Vinkki: findAll)
    Models.Topic.findAll().then(function(topics) {
      res.status(200).json(topics);
    });
});

// GET /topics/:id
router.get('/:id', function(req, res, next) {
  // Hae aihealue tällä id:llä tässä (Vinkki: findOne)
  var topicId = req.params.id;
  Models.Topic.findOne({
    where: {id: topicId},
    include: {model: Models.Message,
      include: {model: Models.User}
    }
  }).then(function(topic) {
    res.status(200).json(topic);
  });
});

// POST /topics
router.post('/', authentication, function(req, res, next) {
  // Lisää tämä aihealue
  var topicToAdd = req.body;
  // Palauta vastauksena lisätty aihealue
  Models.Topic.create(topicToAdd).then(function(addedTopic) {
    res.status(200).json(addedTopic);
  });
});

// POST /topics/:id/message
router.post('/:id/message', authentication, function(req, res, next) {
  // Lisää tällä id:llä varustettuun aihealueeseen...
  var topicId = req.params.id;
  var userId = req.session.userId;
  // ...tämä viesti (Vinkki: lisää ensin messageToAdd-objektiin kenttä TopicId, jonka arvo on topicId-muuttujan arvo ja käytä sen jälkeen create-funktiota)
  var messageToAdd = req.body;
  messageToAdd.TopicId = topicId;
  messageToAdd.UserId = userId;
  Models.Message.create(messageToAdd).then(function(message) {
    res.status(200).json(message);
  });
  // Palauta vastauksena lisätty viesti
});

// DELETE /topics
router.delete('/', function(req, res, next) {
  Models.Topic.destroy({
    where: {name: null}
  }).then(function() {
    res.send(200);
  });
});

module.exports = router;

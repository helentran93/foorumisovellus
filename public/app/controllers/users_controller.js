FoorumApp.controller('UsersController', function($scope, $location, Api){
  // Toteuta kontrolleri tähän
  $scope.userLogIn = {};
  
  $scope.logIn = function() {
      Api.login($scope.userLogIn).success(function(loggedIn) {
          $location.path("/");
      }).error(function() {
          $scope.errorMessage = "Väärä käyttäjätunnus tai salasana!";
      });
  };
  
  $scope.newUser = {};
  
  $scope.registerNew = function() {
      Api.register($scope.newUser).success(function(isCreated) {
          $location.path("/");
      }).error(function(notCreated) {
          $scope.errorMessage = notCreated.error;
      });
  };
  
  $scope.match = false;
  
  $scope.compare = function() {
    if($scope.matchpsw === $scope.newUser.password) {
      $scope.match = false;
    } else {
      $scope.match = true;
    }
  };
});

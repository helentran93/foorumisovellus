FoorumApp.controller('ShowTopicController', function($scope, $routeParams, $location, Api){
  // Toteuta kontrolleri tähän
  
  Api.getTopic($routeParams.id).success(function(showTopic) {
    $scope.topic = showTopic;
  });
  
  $scope.newMsg = {};
  
  $scope.addMsg = function() {
    Api.addMessage($scope.newMsg, $scope.topic.id).success(function(message) {
      $location.path("/messages/" + message.id);
    });
  };
});

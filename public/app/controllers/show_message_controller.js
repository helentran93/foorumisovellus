FoorumApp.controller('ShowMessageController', function($scope, $routeParams, Api){
  // Toteuta kontrolleri tähän
  
  Api.getMessage($routeParams.id).success(function(msg){
    $scope.message = msg;
  });
  
  $scope.newReply = {};
  
  $scope.addReply = function() {
    Api.addReply($scope.newReply, $scope.message.id).success(function(reply) {
      $scope.message.Replies.push(reply);
    });
  };
});
